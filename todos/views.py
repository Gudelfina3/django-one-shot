from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list(request):
    # don't plural todo because then it'll confuse it with the other todos/list.html
    todo = TodoList.objects.all()
    context = {
        "todo_list": todo,
    }
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todo = TodoList.objects.get(id=id)
    context = {"todo_object": todo}
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def edit_todo(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todo)
    context = {
        "form": form,
        "todo_object": todo,
    }
    return render(request, "todos/edit.html", context)


def delete_todo(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/create_item.html", context)


def edit_item(request, id):
    todo = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todo)
    context = {
        "form": form,
        "todo_object": todo,
    }
    return render(request, "todos/edit_item.html", context)
